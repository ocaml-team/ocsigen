Format-Specification: http://wiki.debian.org/Proposals/CopyrightFormat?action=recall&rev=179
Packaged-By: Samuel Mimram <smimram@debian.org>
Packaged-Date: Thu, 14 Sep 2006 13:41:04 +0000
Upstream-Source: http://ocsigen.org/install/ocsigen
Upstream-Maintainer: The Ocsigen Team <dev@ocsigen.org>

Files: *
Copyright: © 2005-2008 Vincent Balat
           © 2005-2008 Jérôme Vouillon
           © 2005 Denis Berthod
           © 2005 Nataliya Guts
           © 2007-2008 Gabriel Kerneis
           © 2007-2008 Stéphane Glondu
License: LGPL-2.2+ | other

  This library is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1 of the
  License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  The complete text of the GNU Lesser General Public License can be
  found in `/usr/share/common-licenses/LGPL-2.1'.

  This program is released under the LGPL version 2.1 (see the text
  below) with the additional exemption that compiling, linking, and/or
  using OpenSSL is allowed.

  As a special exception to the GNU Library General Public License,
  you may also link, statically or dynamically, a "work that uses the
  Library" with a publicly distributed version of the Library to
  produce an executable file containing portions of the Library, and
  distribute that executable file under terms of your choice, without
  any of the additional requirements listed in clause 6 of the GNU
  Library General Public License.  By "a publicly distributed version
  of the Library", we mean either the unmodified Library, or a
  modified version of the Library that is distributed under the
  conditions defined in clause 3 of the GNU Library General Public
  License.  This exception does not however invalidate any other
  reasons why the executable file might be covered by the GNU Library
  General Public License.


Files: eliom/*duce*
Copyright: © 2005-2007 Vincent Balat, Alain Frisch

Files: xmlp4/*
Copyright: © 2005-2008 Vincent Balat
           © 2005 Julien Mineraud
           © 2002-2006 INRIA Rocquencourt
           © 2007 Gabriel Kerneis

Files: xmlp4/ohl-xhtml/*
Copyright: © 2004 Thorsten Ohl
           © 2007 Vincent Balat, Gabriel Kerneis

Files: extensions/cgimod.ml
Copyright: © 2007 Jérôme Velleine, Gabriel Kerneis

Files: examples/miniwiki/miniwiki.ml
Copyright: © 2007 Janne Hellsten


Files: http/multipart.ml
Copyright: © 2001 Patrick Doane, Gerd Stolpmann
License: ZLIB

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any
  damages arising from the use of this software.

  Permission is granted to anyone to use this software for any
  purpose, including commercial applications, and to alter it and
  redistribute it freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must
     not claim that you wrote the original software. If you use this
     software in a product, an acknowledgment in the product
     documentation would be appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must
     not be misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
     distribution.


Files: debian/*
Copyright: © 2006-2007 Samuel Mimram <smimram@debian.org>
           © 2007-2008 Stéphane Glondu <steph@glondu.net>
License: GPL-3+
