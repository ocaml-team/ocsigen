#! /bin/sh
#
# Init script written by Samuel Mimram <smimram@debian.org>.
#
### BEGIN INIT INFO
# Provides:          ocsigen
# Required-Start:    $syslog $network $remote_fs
# Required-Stop:     $syslog $network $remote_fs
# Should-Start:      $local_fs
# Should-Stop:       $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: ocsigen web server.
# Description:       ocsigen web server.
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/bin/ocsigen
NAME=ocsigen
DESC=ocsigen
PIDFILE=/var/run/$NAME.pid
DEFAULTFILE=/etc/default/ocsigen

test -x $DAEMON || exit 0

# Include ocsigen defaults if available
if [ -f $DEFAULTFILE ] ; then
	. $DEFAULTFILE
fi

set -e

case "$1" in
  start|force-start)
	if [ "x$LAUNCH_AT_STARTUP" != "xtrue" ] && [ "x$1" = "xstart" ]; then
	    echo "$DEFAULTFILE prevents $NAME to be started (use force-start)."
	    exit 0
	fi
        if [ -r "$PIDFILE" ] && read pid < "$PIDFILE" && ps -p "$pid" > /dev/null 2>&1; then
            echo "$NAME is already running!"
            exit 0
        fi
	echo -n "Starting $DESC: "
        $DAEMON --daemon --pidfile $PIDFILE $DAEMON_OPTS
	echo "$NAME."
	;;
  stop)
	echo -n "Stopping $DESC: "
        for pid in `cat $PIDFILE`; do kill $pid || true; done
        rm -f $PIDFILE
	echo "$NAME."
	;;
  reload)
        echo -n "Reloading $DESC: "
        echo reload > /var/run/ocsigen_command
        echo "$NAME."
        ;;
  restart|force-reload)
	$0 stop
	$0 start
	;;
  status)
        echo -n "Status of $DESC: "
        if [ ! -r "$PIDFILE" ]; then
            echo "$NAME is not running."
            exit 3
        fi
        if read pid < "$PIDFILE" && ps -p "$pid" > /dev/null 2>&1; then
            echo "$NAME is running."
            exit 0
        else
            echo "$NAME is not running but $PIDFILE exists."
            exit 1
        fi
        ;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|restart|reload|force-reload|force-start|status}" >&2
	exit 1
	;;
esac

exit 0
