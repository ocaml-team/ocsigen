Ocsigen

------------------------------------------------------------------
Instructions:

---
Requirements: 
ocaml (Version >= 3.11 but may work with some
      earlier versions with limited features)
camlp4 (and camlp4rf etc.)
ocamlduce recommanded (otherwise you won't have all the features)
  (same version as ocaml)
findlib (ocamlfind)
lwt >= 2.1
obrowser
pcre-ocaml
ocamlnet >= 2.2.0 (actually only modules netstring, netsys, pcre) 
ocamlssl (sometimes called libssl-ocaml) >= 0.4.4
ocamlsqlite3 >= 0.21.0 (needs sqlite)
(You may also use the dbm database (for example gdbm) instead of sqlite.
 In that case, you need dbm with headers (libgdbm-dev package for ex))
camlzip (libzip-ocaml)
cryptokit
A C compiler

If ocaml/ocamlduce/ocamlfind/ocamlnet/ocamlssl... 
are not installed on your computer, 
you can use GODI to install them automatically.
See http://godi.ocaml-programming.de


---
To compile Ocsigen:

Run 
  
  sh configure [options]

Print the list of possible options using:
./configure --help

Warning: Ocamlduce is now enabled by default.
If you do not have ocamlduce installed, do
  sh configure --disable-ocamlduce

(For example if you have an old version of ocamlnet (< 2.0) you need
./configure --disable-newocamlnet
Use this option if you get a message telling that netsys.cma is missing)

(For example if you want to install all Ocsigen files in a separate directory:
./configure --root /mydirectory --prefix "" --libdir /mylibdir --ocsigen-user myuser --ocsigen-group mygroup
Note that /mylibdir is relative to /mydirectory (--root value))

        If you want to compile a native code version of Ocsigen, you'll need
        a modified version of OCaml, that allows dynlinking of
        native code modules 
        (natdynlink branch in OCaml's CVS, by Alain Frisch).
        It will be included in OCaml 3.11.0.
        If you have it, use ./configure --enable-nativecode


Verify that Makefile.config is OK 

Then, type
  make depend
  make 

To generate the documentation:
  make doc

If you want system-wide installation, become root, and do
  make install
and possibly
  make logrotate
if you want logrotate (rotation of logs) configuration in /etc/logrotate.d
(highly recommanded)

(make installnodoc -> to install without the documentation
 make uninstall -> uninstall (almost) everything)

Adapt the config file to set the port, the modules you want to load
(by default the config file is /etc/ocsigen/ocsigen.conf,
generated from files/ocsigen.conf.in,
but you can specify another one: ocsigen -c configfile)
Set the user and group that will run ocsigen and 
verify he has read access to
 - the modules you load, 
 - to the config file, 
 - and rwx access to log directory.


To run the web server:

  ocsigen

Then you can connect to the server with your browser:

http://localhost

If you are not root, you cannot use port 80. 
Modify the config file to set for example port 8080 for you server.
(and use http://localhost:8080)

If you did not do make install, you can run for example
CAML_LD_LIBRARY_PATH=server server/ocsigen -c ocsigen.conf.local
that launch a server on port 8080 with user permissions on local directory
(ocsigen.conf.local is a sample that may be overwritten by make. 
Copy it if you want to modify it).

Be aware that the server needs C stubs which are compiled into the
shared library dllocsigen.so. It is built in the server directory, and
this location must be documented in the CAML_LD_LIBRARY_PATH
environment variable when dllocsigen.so is not installed in a
system-wide location.

If it does not work, look at the logs or run ocsigen with options -v or -V
(verbose and debug mode)

See the file modules/tutorial.ml for a tutorial or http://www.ocsigen.org

If you make install, it will generate and install the documentation.
Otherwise, you can generate it in the doc directory by typing:
make doc

By default, the tutorial is configured at URL /
and Miniwiki at URL /miniwiki


------------------------------------------------------------------
For debugging purpose, there is now a toplevel with all ocsigen
libraries loaded (very experimental). Example of use:

CAML_LD_LIBRARY_PATH=server ledit ./server/ocsigentop -I /usr/local/lib/ocaml/3.10.1/lwt -I /usr/local/lib/ocaml/3.10.1/ocsigen -I +camlp4 -I +camlp4/Camlp4Parsers -I ./baselib -I ./server
        Objective Caml version 3.10.1

# #use "topfind";;
- : unit = ()
# Ocsigen_loader.add_ocamlpath "/usr/local/lib/ocsigen/METAS";;
- : unit = ()



------------------------------------------------------------------
Ocsigen
------------------------------------------------------------------
Written by:

Vincent Balat
 (project coordinator, Web server, Ocsigenmod, Eliom, Staticmod, xhtml syntax extension, documentation, Ocsimore, extension mechanism, Ocsidbm, Ocsipersist with DBM ...) 
J�r�me Vouillon
 (lwt library, Web server) 
Gabriel Kerneis
 (Xhtml syntax extension for OCaml 3.10, Ocsipersist with SQLite, CGI module, forms in Eliom, deflatemod ...) 
Denis Berthod
 (http protocol, Web server) 
Piero Furiesi
 (Ocsimore) 
Jaap Boender
 (Ocsimore) 
Thorsten Ohl
 (most of the functions generating xhtml (xML and xHTML modules)) 
Nataliya Guts
 (Web server, HTTPS) 
J�r�me Velleine
 (CGI module)

Acknowledgements:

Emmanuel Chailloux
Janne Hellsten
Juliusz Chroboczek
Jean-Vincent Loddo
J�r�me Vouillon
Gr�goire Henry
Alain Frisch
Christophe Fouquer�
Roberto Di Cosmo
Alexandre Miquel
Russ Harmer
Pierre Clairambault
Samuel Mimram
Jaap Boender

and thanks to my (other) students who took part to the very first version:

Yann Binot
Julien Chapuis
Arnaud Delattre
Alexandre Erisay
Julien Mineraud (Xhtmlpp - xhtml pretty-printing)

